﻿using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace GoogleAnalyticsService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            var scopes = new string[] { AnalyticsReportingService.Scope.Analytics, AnalyticsReportingService.Scope.AnalyticsReadonly };
            var service = GetAnalyticsReportingService("193624819112-e12lie0kbhihho79j3pfe0516p2i4s34.apps.googleusercontent.com", "JnzHoGW2Ztgh_DacsjkzlUwR", "Web NCSW Test", scopes);
            var body = BodySetup("2019-06-09","2019-06-14");
            var result = BatchGet(service, body);
            var data = result.Reports[0].Data;

            return View();
        }

        public ActionResult authorize() {
            return View();
        }

        public GetReportsRequest BodySetup(string startDate, string endDate) {
            //Create dimentions
            Dimension dimensionDate = new Dimension
            {
                Name = "ga:date"
            };
            Dimension dimensionSegment = new Dimension
            {
                Name = "ga:segment"
            };
            List<Dimension> dimensions = new List<Dimension>() { dimensionDate,dimensionSegment };
            //Create Metrics
            Metric metric = new Metric {
                Expression = "ga:sessions"
            };
            List<Metric> metrics = new List<Metric>() { metric };
            //Create Segment
            Segment segment = new Segment()
            {
                SegmentId = "gaid::-1"
            };
            List<Segment> segments = new List<Segment>() { segment };
            //Create DateRange
            DateRange dateRange = new DateRange() { StartDate = startDate, EndDate = endDate };
            List<DateRange> dateRanges = new List<DateRange>() { dateRange };
            //Create requests
            ReportRequest reportRequest = new ReportRequest
            {
                Dimensions = dimensions,
                Segments = segments,
                Metrics = metrics,
                ViewId = "196729644",
                DateRanges = dateRanges
            };
            List<ReportRequest> reportRequests = new List<ReportRequest>() { reportRequest };
            //Create and return body request
            var body = new GetReportsRequest() { ReportRequests = reportRequests };
            return body;
        }

        /// <summary>
        /// Returns the Analytics data. 
        /// Documentation https://developers.google.com/analyticsreporting/v4/reference/reports/batchGet
        /// Generation Note: This does not always build corectly.  Google needs to standardise things I need to figuer out which ones are wrong.
        /// </summary>
        /// <param name="service">Authenticated Analyticsreporting service.</param>  
        /// <param name="body">A valid Analyticsreporting v4 body.</param>
        /// <returns>GetReportsResponseResponse</returns>
        public GetReportsResponse BatchGet(AnalyticsReportingService service, GetReportsRequest body)
        {
            try
            {
                // Initial validation.
                if (service == null)
                    throw new ArgumentNullException("service");
                if (body == null)
                    throw new ArgumentNullException("body");

                // Make the request.
                return service.Reports.BatchGet(body).Execute();
            }
            catch (Exception ex)
            {
                throw new Exception("Request Reports.BatchGet failed.", ex);
            }
        }


        /// <summary>
        /// ** Installed Aplication only ** 
        /// This method requests Authentcation from a user using Oauth2.  
        /// </summary>
        /// <param name="clientSecretJson">Path to the client secret json file from Google Developers console.</param>
        /// <param name="userName">Identifying string for the user who is being authentcated.</param>
        /// <param name="scopes">Array of Google scopes</param>
        /// <returns>AnalyticsreportingService used to make requests against the Analyticsreporting API</returns>
        public AnalyticsReportingService GetAnalyticsReportingService(string ClientId, string ClientSecret, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(ClientId))
                    throw new ArgumentNullException("ClientId");
                if (string.IsNullOrEmpty(ClientSecret))
                    throw new ArgumentNullException("ClientSecret");

                var cred = GetUserCredential(ClientId, ClientSecret, userName, scopes);
                return GetService(cred);

            }
            catch (Exception ex)
            {
                throw new Exception("Get Analyticsreporting service failed.", ex);
            }
        }

        /// <summary>
        /// ** Installed Aplication only ** 
        /// This method requests Authentcation from a user using Oauth2.  
        /// Credentials are stored in System.Environment.SpecialFolder.Personal
        /// Documentation https://developers.google.com/accounts/docs/OAuth2
        /// </summary>
        /// <param name="clientSecretJson">Path to the client secret json file from Google Developers console.</param>
        /// <param name="userName">Identifying string for the user who is being authentcated.</param>
        /// <param name="scopes">Array of Google scopes</param>
        /// <returns>authencated UserCredential</returns>
        private UserCredential GetUserCredential(string ClientId, string ClientSecret, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(ClientId))
                    throw new ArgumentNullException("ClientId");
                if (string.IsNullOrEmpty(ClientSecret))
                    throw new ArgumentNullException("ClientSecret");

                // These are the scopes of permissions you need. It is best to request only what you need and not all of them               
                string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                // Requesting Authentication or loading previously stored authentication for userName
                var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets
                {
                    ClientId = ClientId,
                    ClientSecret = ClientSecret
                },
                                                                         scopes,
                                                                         userName,
                                                                         CancellationToken.None,
                                                                         new FileDataStore(credPath, true)).Result;

                credential.GetAccessTokenForRequestAsync();
                return credential;
            }
            catch (Exception ex)
            {
                throw new Exception("Get user credentials failed.", ex);
            }
        }

        /// <summary>
        /// ** Installed Aplication only ** 
        /// This method requests Authentcation from a user using Oauth2.  
        /// </summary>
        /// <param name="clientSecretJson">Path to the client secret json file from Google Developers console.</param>
        /// <param name="userName">Identifying string for the user who is being authentcated.</param>
        /// <param name="scopes">Array of Google scopes</param>
        /// <returns>AnalyticsreportingService used to make requests against the Analyticsreporting API</returns>
        public AnalyticsReportingService GetAnalyticsReportingService(string clientSecretJson, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");
                if (!System.IO.File.Exists(clientSecretJson))
                    throw new Exception("clientSecretJson file does not exist.");

                var cred = GetUserCredential(clientSecretJson, userName, scopes);
                return GetService(cred);

            }
            catch (Exception ex)
            {
                throw new Exception("Get Analyticsreporting service failed.", ex);
            }
        }

        /// <summary>
        /// ** Installed Aplication only ** 
        /// This method requests Authentcation from a user using Oauth2.  
        /// Credentials are stored in System.Environment.SpecialFolder.Personal
        /// Documentation https://developers.google.com/accounts/docs/OAuth2
        /// </summary>
        /// <param name="clientSecretJson">Path to the client secret json file from Google Developers console.</param>
        /// <param name="userName">Identifying string for the user who is being authentcated.</param>
        /// <param name="scopes">Array of Google scopes</param>
        /// <returns>authencated UserCredential</returns>
        private UserCredential GetUserCredential(string clientSecretJson, string userName, string[] scopes)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");
                if (!System.IO.File.Exists(clientSecretJson))
                    throw new Exception("clientSecretJson file does not exist.");

                // These are the scopes of permissions you need. It is best to request only what you need and not all of them               
                using (var stream = new FileStream(clientSecretJson, FileMode.Open, FileAccess.Read))
                {
                    string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    credPath = Path.Combine(credPath, ".credentials/", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                    // Requesting Authentication or loading previously stored authentication for userName
                    var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets
                    {
                        ClientId = "PUT_CLIENT_ID_HERE",
                        ClientSecret = "PUT_CLIENT_SECRET_HERE"
                    },
                                                                             scopes,
                                                                             userName,
                                                                             CancellationToken.None,
                                                                             new FileDataStore(credPath, true)).Result;

                    credential.GetAccessTokenForRequestAsync();
                    return credential;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Get user credentials failed.", ex);
            }
        }

        /// <summary>
        /// This method get a valid service
        /// </summary>
        /// <param name="credential">Authecated user credentail</param>
        /// <returns>AnalyticsreportingService used to make requests against the Analyticsreporting API</returns>
        private static AnalyticsReportingService GetService(UserCredential credential)
        {
            try
            {
                if (credential == null)
                    throw new ArgumentNullException("credential");

                // Create Analyticsreporting API service.
                return new AnalyticsReportingService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analyticsreporting Oauth2 Authentication Sample"
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Get Analyticsreporting service failed.", ex);
            }
        }
    }
}
